aiohipchat
==========

**This is still early alpha, the API will be changing before release**

This is a simple bot for HipChat enabling easy extensibility without having to know much (if anything) about how the HipChat API is implemented, this system is focused around webhooks (since HipChat doesn't yet have a real-time messaging API). All actions are responses to messages (emulated "slash" commands, listening to specific messages based on regex, or other events).

While the system can function as a library to build bots manually - the real usage is via plugins.

There are two main uses of this system:

1. Just use it as a framework (see ``examples/framework_example.py``), basically decorating handler functions for regular expressions (e.g. each message can only be handled by 1 handler)
    * Note: This listens **only to specified message formats**, meaning the bot only ever gets specific messages from atlassian that you define the regex patterns for.
2. Run the base system (installed as the ``hipbot`` command) and provide plugins to load. Essentially allowing the bot to listen to all messages and delegate out tasks to plugins (e.g. the same message could be handled multiple times!)
    * Note: This listens to **all messages** and internally determines how to delegate them based on Python's regex engine.

While this framework does not provide a mechanism to persist any chat history directly, if you are overly concerned about message contents being accessed by other plugins you should choose option 1 (this puts more work on you and you lose out on pre-built plugins, however).

Usage: As a Framework
=====================

Using ``aiohipchat`` as a framework is fairly simple - you basically instantiate a bot and then decorate handlers for specific patterns.

This usage will disable the Plugin API (as the plugin API needs to receive all messages for the channels/teams the bot is installed for, this usage only listens for exact matches).

.. code-block:: python

    from aiohipchat import BotConfig, HipChatBot

    bot = HipChatBot(BotConfig())

    @bot.on_message_received(pattern='/weather \d{5}')
    async def handle_weather(event):
        # event is the raw json body from hipchat (temporarily)
        # the response needs to be the hipchat expected format (temporarily)
        weather = await get_weather_by_zip(event)
        return {
            'message': 'Weather for zip {} is {}'.format(resolve_zip(event),
                                                         weather)
        }

A few notes on handler specifics:

- Handler functions **must be async**. This is to ensure that you are at least aware that you are operating in an event loop, so be mindful to not block the loop with CPU intensive tasks. If you need something long running, use `loop.run_in_executor` - just note that hipchat wants a response within 3s if you want to display a response in the sending channel.
- Handler **function names are important**, they are used to generate a unique url (``/aiohipchat/<event_name>/<handler_name>``). Duplicates will be non-deterministically prioritized (but you will be warned). If you change a function name, you will need to update the plugin in hipchat so it re-registers the urls.
- The event and response api will be cleaned up as the usage becomes more clear.
- You do not have to worry about serialization or deserialization, just be sure to respond with JSONable formats.

Optional Dependencies
---------------------

- ``pyjwt``: Required if you use a ``aiohipchat.auth.CredentialStorageStrategy`` (**highly recommended**)
- ``aioredis``: Required if you use the default ``aiohipchat.auth.redis.RedisPoolStrategy``

Usage: Standalone
=================

This usage method is much friendlier if you just want to use features provided by either default plugins or anything third party (assuming anyone writes any ;)).

It's as simple as this:

1. ``pip install aiohipchat[standalone]``: Install the library and core dependencies, along with an command called ``hipbot``.
2. Start the bot server with ``hipbot``
3. Go to ``https://<mycompany>.hipchat.com/addons/``
4. Click the link at the bottom of the page that says "Install an integration from a descriptor URL"
5. Enter ``https://<wherever you are running aiohipchat>.com/aiohipchat/capabilities``
6. Accept the popup (oauth permissions grant)

Optional Dependencies
---------------------

Most of these are recommended, to ensure that the server is more secure:

- ``pyjwt``: Token validation from hipchat (**highly recommended**)
- ``aioredis``: Persistence for the user credentials

Of course, you should be serving with SSL - otherwise this is effectively moot.

Extending the System
--------------------

**Note**: While the bot core is async, your plugin is not required to be, it will be run in a separate process. This is to avoid a misbehaving plugin from blocking the event loop.

Plugins
-------

If you don't care to manually use "the bot" with decorators, you can easily extend the functionality with the plugin system and just not touch the core, allowing you to run the standard bot command from your terminal: ``hipbot``.

Plugins are resolved via entry_points and just need to define a class (or series of classes) that implement AbstractPlugin_ (note, we don't check the instance and embrace the ducktyping).

Sample ``setup.py`` file:

.. code-block:: python

    from setuptools import setup, find_package

    setup(name='aiohipchat-pluginname',
          ...,
          entry_points={
              'aiohipchat.plugin' : [
                  'pluginname = the.package.path:ClassPlugin',
              ]
          }
    )

On boot the bot will scan for plugins, automatically registering them with the system. To enable them with your hipchat installation, you will need to use the [todo] plugin customization url.

.. _aiohttp: https://github.com/KeepSafe/aiohttp
.. _AbstractPlugin: TODO
