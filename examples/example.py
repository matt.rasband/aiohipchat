import logging

from aiohipchat import HipChatBot, BotConfig
from aiohipchat.auth import InMemoryStrategy


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    cfg = BotConfig(name='testbot', vendor_name='Matt Rasband',
                    vendor_url='https://gitlab.com/matt.rasband/aiohipchat',
                    homepage='https://gitlab.com/matt.rasband/aiohipchat',
                    description=('Bot for HipChat to mirror some Slack-like'
                                 ' actions.'),
                    scopes=['view_room', 'view_messages'], allow_global=True)

    hipchat = HipChatBot(cfg, auth_strategy=InMemoryStrategy())

    @hipchat.on_room_message(pattern='/hello (?P<name>\w+)$')
    async def handle_hello(event) -> dict:
        print('event:', event)
        return {
            'from': '',  # optional, puts info next to username
            'message_format': 'text',  # or
            'message': 'foobar',  #
        }

    hipchat.run()
