from setuptools import setup, find_packages
import sys


if sys.version_info < (3, 5):
    print('aiohipbot requires Python 3.5+', file=sys.stderr)
    sys.exit(1)


setup(name='aiohipchat',
      version='0.1.0',
      description='Pluggable bot framework for Atlassian\'s HipChat',
      author='Matt Rasband',
      author_email='matt.rasband@gmail.com',
      maintainer='Matt Rasband',
      maintainer_email='matt.rasband@gmail.com',
      license='Apache-2.0',
      url='https://gitlab.com/matt.rasband/aiohipchat',
      download_url='https://gitlab.com/matt.rasband/aiohipchat',
      keywords=[
          'hipchat',
          'bot',
          'chatbot',
          'asyncio',
          'aiohttp',
      ],
      packages=find_packages(),
      classifiers=[
          'Programming Language :: Python :: 3.5',
          'License :: OSI Approved :: Apache Software License',
          'Intended Audience :: Developers',
          'Development Status :: 2 - Pre-Alpha',
          'Topic :: Software Development',
      ],
      setup_requires=[
          'pytest-runner',
          'flake8',
      ],
      install_requires=[
          'aiohttp>=1.0',
          'gunicorn',
      ],
      extras_require={
          # pip install aiohipchat[recommends]
          'recommends': ['ujson', 'uvloop', 'cchardet', 'aiodns'],
      },
      tests_require=[
          'pytest-aiohttp',
          'pytest',
      ],
      entry_points={
         'console_scripts': [
             'hipbot = aiohipchat.__main__:main'
         ]
      },
      zip_safe=False,
      include_package_data=True)
