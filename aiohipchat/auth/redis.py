from . import CredentialStorageStrategy


class RedisPoolStrategy(CredentialStorageStrategy):
    def __init__(self, pool):
        self._pool = pool

    async def save(self, oauth2_creds: dict) -> None:
        return super().save(oauth2_creds)

    async def delete(self, oauth2_client_id: str) -> None:
        return super().delete(oauth2_client_id)

    async def get(self, oauth2_client_id: str) -> dict:
        return super().get(oauth2_client_id)
