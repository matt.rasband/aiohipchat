from abc import ABC, abstractmethod
import contextlib
import json
from pathlib import Path

import jwt


class CredentialStorageStrategy(ABC):
    """
    https://developer.atlassian.com/hipchat/guide/installation-flow/server-side-installation
    """
    @abstractmethod
    async def save(self, oauth2_creds: dict) -> None:
        """
        Save the oauth2 creds from hipchat. These are only
        ever given on installation, and need to be saved
        in order to both validate incoming messages and
        send notifications without it being in direct
        response to a

        :param oauth2_creds: Atlassian oauth2 creds.
                            They look like:

                             {
                                oauthId: str,
                                capabilitiesUrl: str,
                                roomId: Optional<int>,
                                groupId: int,
                                oauthSecret: str,
                             }
        :return: None
        """

    @abstractmethod
    async def delete(self, oauth2_client_id: str) -> None:
        """
        Delete the oauth2 credentials.

        :param oauth2_client_id:
        :return: None
        """

    @abstractmethod
    async def get(self, oauth2_client_id: str) -> dict:
        """
        Get client credentials based on the oauth id

        :param oauth2_client_id: client id
        :return: Credentials, looks like what was saved.
        """

    async def authenticate(self, auth_header: str) -> bool:
        """
        Check if the auth header is valid per the hipchat protocol.

        :param auth_header: Content of the 'Authorization' header,
                            this should be the full string unmodified.
        :return: boolean
        """
        auth_prefix = 'JWT '
        if not auth_header.startswith(auth_prefix):
            return False  # Invalid token format

        token = auth_header.replace(auth_prefix, '')

        # First we need to resolve the issuer (client id) from the JWT
        # This is used to get the secret we got on install for validation
        insecure_jwt = jwt.decode(token, verify=False)
        creds = await self.get(insecure_jwt['iss'])
        if creds is None:
            return False  # Unknown client

        try:
            jwt.decode(token, key=creds['oauthSecret'])
        except (jwt.ExpiredSignatureError, jwt.InvalidTokenError):
            return False  # Bad/Expired/Tampered with token
        return True


class InMemoryStrategy(CredentialStorageStrategy):
    """
    Token auth strategy implementation using an in-memory
    dictionary. Note that this is transient and credentials
    will be lost on server restart. It should only be used
    for local development and testing.
    """
    def __init__(self):
        self._creds = {}  # type: Dict[str, str]

    async def save(self, oauth2_creds: dict):
        self._creds[oauth2_creds['oauthId']] = oauth2_creds

    async def delete(self, oauth2_client_id: str):
        try:
            del self._creds[oauth2_client_id]
        except KeyError:
            pass

    async def get(self, oauth2_client_id: str):
        try:
            return self._creds[oauth2_client_id]
        except KeyError:
            return None


class FileStrategy(CredentialStorageStrategy):
    def __init__(self, directory: Path):
        self._dir = directory

    def _file(self, id_):
        return self._dir / (id_ + '.json')

    async def save(self, oauth2_creds: dict):
        with open(str(self._file(oauth2_creds['oauthId'])), 'w') as f:
            creds = json.dumps(oauth2_creds)
            f.write(creds)

    async def delete(self, oauth2_client_id: str):
        with contextlib.suppress(IOError):
            self._file(oauth2_client_id).unlink()

    async def get(self, oauth2_client_id: str):
        try:
            with open(str(self._file(oauth2_client_id))) as f:
                return json.load(f)
        except IOError:
            return None
