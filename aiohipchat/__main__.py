from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import importlib.util
import multiprocessing

import gunicorn.app.base
from gunicorn.six import iteritems

from . import HipChatBot, BotConfig


class StandaloneApplication(gunicorn.app.base.BaseApplication):
    """
    Customize the Gunicorn application runner to delay instantiation
    of the framework with an app_factory. This is so the runner can
    dynamically configure which runner to use depending on the
    available event loop implementations.
    """
    def __init__(self, app_factory, options=None):
        """
        App definition for Gunicorn's usage.
        :param app_factory: Callable to generate the application object.
        :param options: Gunicorn options (basically look at the CLI)
        """
        self.options = options or {}
        self.app_factory = app_factory
        super().__init__()

    def init(self, parser, opts, args):
        pass

    def load_config(self):
        for key, value in iteritems(self.options):
            if key.lower() in self.cfg.settings and value is not None:
                self.cfg.set(key.lower(), value)

    def load(self):
        """Lazy loads the application instance"""
        return self.app_factory()


def num_workers():
    """Get the number of workers for the system, this
    uses the cpu_count (usually cores - *2 if hyper-threaded)
    + 1 (per the gunicorn recommended spec)"""
    return multiprocessing.cpu_count() + 1


def parse_args():
    """
    Parse the command line arguments, showing the help info if the user
    provides the `-h/--help`
    :return: Parsed arguments
    """
    parser = ArgumentParser(description='Run the aiohipchat bot prod server',
                            formatter_class=ArgumentDefaultsHelpFormatter)

    subcommands = parser.add_subparsers(dest='subcommand')

    run_parser = subcommands.add_parser('run',
                                        help='Commands to run the bot.')
    run_parser.add_argument('-b', '--bind', default='0.0.0.0:8080',
                            type=str, help='Server listen address.')
    run_parser.add_argument('--reload', action='store_true',
                            help='Enable server reload in source changes')
    run_parser.add_argument('--workers', default=num_workers(), type=int,
                            help='Number of web workers to run.')

    return parser.parse_args()


def main():
    args = parse_args()

    if args.subcommand == 'run':
        options = {
            'bind': args.bind,
            'workers': args.workers,
            'reload': args.reload,
            'worker_class': 'aiohttp.worker.GunicornWebWorker',
        }

        if importlib.util.find_spec('uvloop') is not None:
            options['worker_class'] = 'aiohttp.worker.GunicornUVLoopWebWorker'

        def app_factory():
            config = BotConfig('hipbot', 'Matt Rasband',
                               'https://gitlab.com/matt.rasband/aiohipchat')
            return HipChatBot(bot_config=config).app

        StandaloneApplication(app_factory, options).run()


if __name__ == '__main__':
    main()
