from abc import ABC, abstractmethod
from pkg_resources import iter_entry_points


def load_plugins(group):
    """
    Resolve and load all plugins in the group, the plugins
    will be "loaded" before they are initialized, but they
    are not
    :param group:
    :return:
    """
    for plug in iter_entry_points(group=group):
        plugin = plug.load()
        yield plugin


class Plugin(ABC):
    __PATTERN__ = None
    __NAME__ = None

    @abstractmethod
    async def on_message(self, message) -> dict:
        pass
