import asyncio
from collections import namedtuple
from functools import wraps
from http import HTTPStatus
import logging
from typing import Optional, List, Dict, AnyStr, Any

from aiohttp import web

from .auth import CredentialStorageStrategy

logger = logging.getLogger('aiohipchat')

_Route = namedtuple('_Route', 'method path func')


class BotConfig:
    """
    Configuration for the bot, anything required is positional, the rest
    may be filled in with sensible defaults if they are expected.
    """
    __slots__ = ('name', 'vendor_name', 'vendor_url',
                 'description', 'key', 'homepage',
                 'bot_name', 'allow_global', 'allow_room',
                 'scopes')

    def __init__(self, name: str, vendor_name: str, vendor_url: str, *,
                 description: str = '', key: Optional[str] = None,
                 homepage: str = '', bot_name: Optional[str] = None,
                 allow_global: bool = False, allow_room: bool = True,
                 scopes: Optional[List] = None):
        """
        :param name: Integration name, his is shown in the integrations menu
        :param vendor_name: Maker of the bot
        :param vendor_url: URL for the maker
        :param description: Bot description, shown in the integrations
        :param key: Marketplace key, defaults to name lowercase
        :param homepage: Bot description homepage
        :param bot_name: Name of the bot, defaults to name if not set
        :param allow_global: Allow the bot to be a global integration
        :param allow_room: Allow the bot to be installed per room
        :param scopes: Override our default given scopes ['view_messages']

        See:
        https://developer.atlassian.com/hipchat/guide/hipchat-rest-api/api-scopes  # noqa
        """
        self.name = name
        self.vendor_name = vendor_name
        self.vendor_url = vendor_url
        self.description = description
        self.key = key or name.replace(' ', '-').lower()
        self.homepage = homepage
        self.bot_name = bot_name or name
        self.allow_global = allow_global
        self.allow_room = allow_room
        self.scopes = scopes or ['view_messages']


class HipChatBot:
    """
    Hipchat's integrations are, as of 2016/09, all built around either
    installed Java extensions (Atlassian SDK), webhooks (e.g. POST to
    registered listeners based on a regex), or client polling based.

    This is primarily built around the webhook events to simulate somewhat
    real-time feedback loops. However, it will also store the credentials
    (via the auth_strategy), and can be extended to easily post messages
    to arbitrary channels based on external stimuli and known specific
    channels.

    Note that the top level path '/aiohipchat/' is reserved for the bot's
    needs, but otherwise routes are up for grabs.
    """
    def __init__(self, bot_config, *,
                 auth_strategy: CredentialStorageStrategy = None):
        self._bot_config = bot_config
        self._auth_strategy = auth_strategy

        self._app = web.Application()
        self._app.on_startup.append(self._register_routes)

        self._capabilities = None  # type: Dict
        self._webhooks = []  # type: [Dict[AnyStr, AnyStr]]
        self._routes = []  # type: [_Route]

    def run(self, *, host='0.0.0.0', port=8080):
        """
        Run the bot (webserver), making it ready to serve requests.

        :param host: Host interface to listen on
        :param port: Port to listen on
        :return:
        """
        web.run_app(self._app, host=host, port=port)

    @property
    def app(self):
        """Expose the app property to simplify gunicorn deployment."""
        return self._app

    def capabilities(self, request: web.Request) -> Dict[AnyStr, Any]:
        """
        Lazily initialize the capabilities of the bot. Basically this is a
        manifest format that hipchat uses to know what it can call in your
        external service.

        :param request: Request to resolve the capabilities
        :return: Dictionary describing the bot's manifest/capabilities for
                 hipchat
        """
        if not self._capabilities:
            hostname = self._resolve_host(request)

            logger.debug('Generating capabilities, using hostname %s',
                         hostname)

            router = request.app.router
            capabilities_url = router['capabilities'].url()
            installed_cb_url = router['installed_callback'].url()

            self._capabilities = {
                'name': self._bot_config.name,
                'description': self._bot_config.description,
                'key': self._bot_config.key,
                'links': {
                    'homepage': self._bot_config.homepage,
                    'self': hostname + capabilities_url,
                },
                'vendor': {
                    'name': self._bot_config.vendor_name,
                    'url': self._bot_config.vendor_url,
                },
                'capabilities': {
                    'webhook': self._webhooks,
                    'hipchatApiConsumer': {
                        'fromName': self._bot_config.bot_name,
                        'scopes': self._bot_config.scopes,
                    },
                    'installable': {
                        'allowGlobal': self._bot_config.allow_global,
                        'allowRoom': self._bot_config.allow_room,
                        'callbackUrl': hostname + installed_cb_url,
                    }
                },
            }
            for hook in self._webhooks:
                hook['url'] = hostname + hook['url']
                logger.debug('Rewrote webhook to %s', hook['url'])
        return self._capabilities

    def _register_routes(self, app: web.Application) -> None:
        """
        Register all the routes that the bot can serve, this
        includes routes for the installation callback handler
        and the capabilities resolution handler - along with
        all the decorated functions/plugin classes.

        :param app: aiohttp application instance
        :return: None
        """
        logger.debug('Adding route to handle installation callback.')
        app.router.add_post('/aiohipchat/installed',
                            self._installed_callback_handler,
                            name='installed_callback')
        logger.debug('Adding route to handle capabilities query')
        app.router.add_get('/aiohipchat/capabilities',
                           self._capabilities_handler,
                           name='capabilities')

        known_routes = {route.url() for route in
                        app.router.named_resources().values()}
        for route in self._routes:
            logger.debug('Adding route %s to app', route)
            if route.path in known_routes:
                raise ValueError(('Duplicate route path registered as "{}" on '
                                  'handler "{}", you need to customize the '
                                  'path if you have more than one handler'
                                  ' for any given event.')
                                 .format(route.path, route.func.__name__))
            known_routes.add(route.path)
            app.router.add_route(route.method, route.path, route.func)

    @staticmethod
    def _resolve_host(request: web.Request) -> str:
        """
        Resolve the hostname from the request object,
        used to dynamically determine what scheme/host/port
        we are listening on, rather than relying on a config.

        :param request: http request
        :return: str
        """
        return '{}://{}'.format(request.scheme, request.host)

    def _register_webhook(self, event, *, pattern=None):
        """
        Registers a webhook with the capabilities handler.

        The http path will be created dynamically and will compose
        the name of the function wrapped. If a function's name is
        changed the plugin will need to be re-installed with the
        hipchat instance.

        :param event: Which event type the handler listens to
                      (see HipChat's docs)
        :param pattern: If the event type allows for a pattern, a regex for
                        atlassian (e.g. it needs to work in Java)
        :return: decorated handler function, registered to receive events.
        """
        def decor(f):
            if not asyncio.iscoroutinefunction(f):
                raise ValueError('Provided function ' + f.__name__ + ' is not'
                                 'a coroutine')

            # Create the webhook capability description for the manifest.
            path = '/aiohipchat/{event}/{func_name}' \
                .format(event=event, func_name=f.__name__)
            webhook = {
                'url': path,  # host will be prepended on demand
                'event': event,  # hipchat event definition
                'authentication': 'none' if self._auth_strategy is None
                                  else 'jwt',
                'name': path,
                'key': path.replace('/', '-'),
            }
            if pattern:
                webhook.update(dict(pattern=pattern))
            self._webhooks.append(webhook)

            @wraps(f)
            async def wrapper(*args):
                logger.debug('Invoking "%s" for event "%s"', f.__name__,
                             event)
                # The two known types of handlers are:
                #  * functions (request is the first param)
                #  * class based views (request is an instance property)
                request = args[0] if isinstance(args[0], web.Request) else \
                    args[0].request  # type: web.Request

                if self._auth_strategy:
                    logger.debug('Auth Strategy present, checking for auth.')
                    auth_header = request.headers.get('Authorization')
                    if not await self._auth_strategy.authenticate(auth_header):
                        logger.debug('Rejecting request, unable to auth.')
                        return web.json_response(
                            status=HTTPStatus.UNAUTHORIZED)

                # TODO: should probably create an event type
                hook_event = await request.json()

                # TODO: should probably define a response type (or types)
                return web.json_response(await f(hook_event))

            self._routes.append(_Route('POST', path, wrapper))

            return wrapper

        return decor

    ######################
    # Default Handlers
    ######################

    async def _installed_callback_handler(self, request: web.Request):
        """
        Handles the installation callback, saving the credentials
        if a auth_strategy was given.
        :param request: aiohttp.web.Request
        :return: aiohttp.web.Response
        """
        logger.debug('Handling installation')
        if self._auth_strategy:
            logger.debug('Auth strategy given, storing credentials.')
            creds = await request.json()
            await self._auth_strategy.save(creds)
        return web.json_response({}, status=HTTPStatus.OK)

    async def _capabilities_handler(self, request: web.Request):
        """
        Handle the capabilities request, basically this provides a
        manifest of webhooks/events/etc. this bot can handle.

        :param request: aiohttp.web.Request
        :return: aiohttp.web.Response
        """
        return web.json_response(self.capabilities(request),
                                 status=HTTPStatus.OK)

    ######################
    # Event Decorators
    # TODO: Create a class option that can register the same callbacks
    # TODO: We can use a decorator to do the boilerplate of calling
    # self._register_webhook
    ######################
    def on_room_archived(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_archived
        """
        return self._register_webhook('room_archived')

    def on_room_created(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_created
        """
        return self._register_webhook('room_created')

    def on_room_deleted(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_deleted
        """
        return self._register_webhook('room_deleted')

    def on_room_enter(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_enter
        """
        return self._register_webhook('room_enter')

    def on_room_exit(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_exit
        """
        return self._register_webhook('room_exit')

    def on_room_file_upload(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_file_upload
        """
        return self._register_webhook('room_file_upload')

    def on_room_message(self, pattern):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_message
        """
        return self._register_webhook('room_message', pattern=pattern)

    def on_room_notification(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_notification
        """
        return self._register_webhook('room_notification')

    def on_room_topic_change(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_topic_change
        """
        return self._register_webhook('room_topic_change')

    def on_room_unarchived(self):
        """
        https://www.hipchat.com/docs/apiv2/webhooks#room_unarchived
        """
        return self._register_webhook('room_unarchived')
